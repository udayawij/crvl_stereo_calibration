#include "crvlStereoRectifier.h"

#define LEFT		(0)
#define RIGHT		(1)

using namespace cv;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
crvlStereoRectifier::crvlStereoRectifier(void)
{
	m_nWidth = m_nHeight = -1;
	m_pStereoImage = NULL;
	m_pRectifiedIntrinsic = m_pIntrinsic[0] = m_pIntrinsic[1] = NULL;
	m_pDistCoef[0] = m_pDistCoef[1] = NULL;
	m_pRotation = NULL;
	m_pTranslation = NULL;
	m_pRotInCameraCoord[0] = m_pRotInCameraCoord[1] = NULL;
	m_pPPM[0] = m_pPPM[1] = NULL;
	m_pMapX[0] = m_pMapX[1] = m_pMapY[0] = m_pMapY[1] = NULL;
	m_pRectifiedImage[0] = m_pRectifiedImage[1] = NULL;
	m_dFocalLength = m_dBaseline = m_dCenterX = m_dCenterY = 0.;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
crvlStereoRectifier::~crvlStereoRectifier(void)
{
	for(int i=0; i<2; i++) {
		if(m_pIntrinsic[i]) {
			cvReleaseMat(&m_pIntrinsic[i]);
			m_pIntrinsic[i] = NULL;
		}

		if(m_pDistCoef[i]) {
			cvReleaseMat(&m_pDistCoef[i]);
			m_pDistCoef[i] = NULL;
		}

		if(m_pRotInCameraCoord[i]) {
			cvReleaseMat(&m_pRotInCameraCoord[i]);
			m_pRotInCameraCoord[i] = NULL;
		}

		if(m_pPPM[i]) {
			cvReleaseMat(&m_pPPM[i]);
			m_pPPM[i] = NULL;
		}

		if(m_pH[i]) {
			cvReleaseMat(&m_pH[i]);
			m_pH[i] = NULL;
		}

		if(m_pMapX[i]) {
			cvReleaseMat(&m_pMapX[i]);
			m_pMapX[i] = NULL;
		}

		if(m_pMapY[i]) {
			cvReleaseMat(&m_pMapY[i]);
			m_pMapY[i] = NULL;
		}

		if(m_pRectifiedImage[i]) {
			cvReleaseImage(&m_pRectifiedImage[i]);
			m_pRectifiedImage[i] = NULL;
		}
	}

	if(m_pStereoImage) {
		cvReleaseImage(&m_pStereoImage);
		m_pStereoImage = NULL;
	}

	if(m_pRotation) {
		cvReleaseMat(&m_pRotation);
		m_pRotation = NULL;
	}

	if(m_pTranslation) {
		cvReleaseMat(&m_pTranslation);
		m_pTranslation = NULL;
	}

	if(m_pRectifiedIntrinsic) {
		cvReleaseMat(&m_pRectifiedIntrinsic);
		m_pRectifiedIntrinsic = NULL;
	}
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
bool
crvlStereoRectifier::Init(int nWidth, int nHeight, char *pCalibrationFilePath)
{
	// 일반변수 초기화
	m_nWidth = nWidth;
	m_nHeight = nHeight;

	// rectification을 위한 스테레오 카메라 보정 정보 읽어오기
	for(int i=0; i<2; i++) {
		m_pIntrinsic[i] = cvCreateMat(3, 3, CV_64F);
		m_pDistCoef[i] = cvCreateMat(1, 5, CV_64F);
		m_pRotInCameraCoord[i] = cvCreateMat(3, 3, CV_64F);
		m_pPPM[i] = cvCreateMat(3, 4, CV_64F);
		m_pH[i] = cvCreateMat(3, 3, CV_64F);
		m_pMapX[i] = cvCreateMat(m_nHeight, m_nWidth, CV_32F);
		m_pMapY[i] = cvCreateMat(m_nHeight, m_nWidth, CV_32F);
		
		cvmSetIdentity(m_pIntrinsic[i]);
		cvmSetZero(m_pDistCoef[i]);
		cvmSetIdentity(m_pRotInCameraCoord[i]);
		cvmSetIdentity(m_pPPM[i]);
		cvmSetIdentity(m_pH[i]);
		cvmSetZero(m_pMapX[i]);
		cvmSetZero(m_pMapY[i]);
	}

	m_pRectifiedIntrinsic = cvCreateMat(3, 3, CV_64F);
	m_pRotation = cvCreateMat(3, 3, CV_64F);
	m_pTranslation = cvCreateMat(3, 1, CV_64F);

	cvmSetIdentity(m_pRectifiedIntrinsic);
	cvmSetIdentity(m_pRotation);
	cvmSetZero(m_pTranslation);

	if(_ParseCalibFile(pCalibrationFilePath)) {
		// stereo rectification
		cvStereoRectify(
			m_pIntrinsic[LEFT], m_pIntrinsic[RIGHT],
			m_pDistCoef[LEFT], m_pDistCoef[RIGHT],
			cvSize(m_nWidth, m_nHeight),
			m_pRotation, m_pTranslation,
			m_pRotInCameraCoord[LEFT], m_pRotInCameraCoord[RIGHT],
			m_pPPM[LEFT], m_pPPM[RIGHT],
			0, 0, 
			-1, 
			cvSize(0, 0));

		_PrintMatrix("Left PPM", m_pPPM[LEFT]);
		_PrintMatrix("Right PPM", m_pPPM[RIGHT]);

		// make a new intrinsic parameter
		for(int r=0; r<3; r++) {
			for(int c=0; c<3; c++) {
				cvmSet(m_pRectifiedIntrinsic, r, c, cvmGet(m_pPPM[LEFT], r, c));
			}
		}

		// store a new camera parameter
		m_dFocalLength = cvmGet(m_pRectifiedIntrinsic, 0, 0);		// (0, 0)이나 (1, 1)은 같은 값이다.
		m_dCenterX = cvmGet(m_pRectifiedIntrinsic, 0, 2);
		m_dCenterY = cvmGet(m_pRectifiedIntrinsic, 1, 2);

		double dTx = cvmGet(m_pTranslation, 0, 0);
		double dTy = cvmGet(m_pTranslation, 1, 0);
		double dTz = cvmGet(m_pTranslation, 2, 0);

		m_dBaseline = sqrt((dTx*dTx) + (dTy*dTy) + (dTz*dTz));		// 베이스 라인 결정해줘야 한다. 어떻게 하지.......
		m_dBaseline *= 0.001;										// mm -> m급으로 저장

		printf("Baseline\n%lf (unit: m)\n\n", m_dBaseline);

		// map creation
		for(int i=0; i<2; i++) {
			cvInitUndistortRectifyMap(m_pIntrinsic[i], m_pDistCoef[i], m_pRotInCameraCoord[i], m_pRectifiedIntrinsic, m_pMapX[i], m_pMapY[i]);
		}

		/*/ 다른 방법으로!
		{
			Mat cameraMatrix1 = Mat(m_pIntrinsic[LEFT]);
			Mat cameraMatrix2 = Mat(m_pIntrinsic[RIGHT]);
			Mat H1 = Mat(m_pH[LEFT]);
			Mat H2 = Mat(m_pH[RIGHT]);
			Mat R1 = cameraMatrix1.inv()*H1*cameraMatrix1;
			Mat R2 = cameraMatrix2.inv()*H2*cameraMatrix2;
			Mat P1 = cameraMatrix1;
			Mat P2 = cameraMatrix2;

			CvMat *_R1 = cvCreateMat(3, 3 ,CV_64F);
			CvMat *_R2 = cvCreateMat(3, 3 ,CV_64F);
			CvMat *_P1 = cvCreateMat(3, 3 ,CV_64F);
			CvMat *_P2 = cvCreateMat(3, 3 ,CV_64F);

			for(int r=0; r<3; r++) {
				for(int c=0; c<3; c++) {
					cvmSet(_R1, r, c, R1.at<double>(r, c));
					cvmSet(_R2, r, c, R2.at<double>(r, c));
					cvmSet(_P1, r, c, P1.at<double>(r, c));
					cvmSet(_P2, r, c, P2.at<double>(r, c));
				}
			}

			cvInitUndistortRectifyMap(m_pIntrinsic[LEFT], m_pDistCoef[LEFT], _R1, _P1, m_pMapX[LEFT], m_pMapY[LEFT]);
			cvInitUndistortRectifyMap(m_pIntrinsic[RIGHT], m_pDistCoef[RIGHT], _R2, _P2, m_pMapX[RIGHT], m_pMapY[RIGHT]);

			cvReleaseMat(&_R1);
			cvReleaseMat(&_R2);
			cvReleaseMat(&_P1);
			cvReleaseMat(&_P2);
		}*/

		_PrintMatrix("Rectified Intrinsic", m_pRectifiedIntrinsic);
	}

	// 영상 버퍼 생성
	for(int i=0; i<2; i++) {
		m_pRectifiedImage[i] = cvCreateImage(cvSize(m_nWidth, m_nHeight), 8, 3);
	}

	// 스테레오 버퍼 생성
	m_pStereoImage = cvCreateImage(cvSize(m_nWidth*2, m_nHeight), 8, 3);

	// 폰트객체 초기화
	cvInitFont(&m_Font, CV_FONT_VECTOR0, 0.6, 0.6, 0, 2);

	return true;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// rectified 된 영상 가져오기
IplImage*
crvlStereoRectifier::GetRectifiedImagePtr(int nCameraIdx)
{
	if(nCameraIdx == 0) {
		return m_pRectifiedImage[LEFT];
	} else {
		return m_pRectifiedImage[RIGHT];
	}
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 좌/우 영상을 붙여서 만들기
// 좌/우 합쳐진 영상 가져오기
IplImage*
crvlStereoRectifier::GetRectifiedStereoImagePtr(bool bFlagDrawEpipolarLine, int nStartY, int nInterval, CvScalar color)
{
	// 영상 그리기
	int nWidth = m_pStereoImage->width / 2;
	int nHeight = m_pStereoImage->height;
	cvSetZero(m_pStereoImage);

	// left 영상
	cvSetImageROI(m_pStereoImage, cvRect(0, 0, nWidth, nHeight));
	cvCopy(m_pRectifiedImage[LEFT], m_pStereoImage);
	cvResetImageROI(m_pStereoImage);

	// right 영상
	cvSetImageROI(m_pStereoImage, cvRect(nWidth, 0, nWidth, nHeight));
	cvCopy(m_pRectifiedImage[RIGHT], m_pStereoImage);
	cvResetImageROI(m_pStereoImage);
	
	// epipolar 라인 그려보기
	if(bFlagDrawEpipolarLine) {
		for(int y=nStartY; y<m_nHeight; y+=nInterval) {
			cvLine(m_pStereoImage, cvPoint(0, y), cvPoint(nWidth*2-1, y), color, 1);
		}
	}

	// 타이머 정지
	m_dTime = (double)cvGetTickCount() - m_dTime;
	m_dTime = m_dTime/(cvGetTickFrequency()*1000);

	sprintf(m_szBuffer, "%g", m_dTime);
	m_dTime = atof(m_szBuffer);

	// 타이머 출력
	sprintf(m_szBuffer, "%.2lfms (%.1lffps)", m_dTime, 1000./m_dTime);
	cvPutText(m_pStereoImage, m_szBuffer, cvPoint(12, 27), &m_Font, CV_RGB(0, 0, 0));
	cvPutText(m_pStereoImage, m_szBuffer, cvPoint(10, 25), &m_Font, CV_RGB(0, 255, 0));

	// 타이머 시작
	m_dTime = (double)cvGetTickCount();

	return m_pStereoImage;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
void
crvlStereoRectifier::MakeRectifiedImage(IplImage *pLeftImage, IplImage *pRightImage)
{
	cvRemap(pLeftImage, m_pRectifiedImage[0], m_pMapX[0], m_pMapY[0]);
	cvRemap(pRightImage, m_pRectifiedImage[1], m_pMapX[1], m_pMapY[1]);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
void
crvlStereoRectifier::_PrintMatrix(char *pMessage, CvMat *pMat)
{
	printf("%s\n", pMessage);

	for(int r=0; r<pMat->rows; r++) {
		for(int c=0; c<pMat->cols; c++) {
			printf("%.12lf ", cvmGet(pMat, r, c));
		} printf("\n");
	} printf("\n");
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
bool
crvlStereoRectifier::_ParseCalibFile(char *pFilePath)
{
	FILE *fp = fopen(pFilePath, "r+");

	if(!fp) {
		printf("crvlStereoRectifier: Failed to open the stereo calibration.\n");
		return false;
	}

	char szBuffer[512];

	while(!feof(fp)) {
		fscanf(fp, "%s", szBuffer);

		if(!strcmp("fc_left", szBuffer)) {
			fscanf(fp, "%s", szBuffer);
			fscanf(fp, "%s", szBuffer);
				
			// fc_left_x
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pIntrinsic[LEFT], 0, 0, atof(szBuffer));

			// fc_left_y
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pIntrinsic[LEFT], 1, 1, atof(szBuffer));
			continue;
		} else if(!strcmp("cc_left", szBuffer)) {
			fscanf(fp, "%s", szBuffer);
			fscanf(fp, "%s", szBuffer);
			
			// cc_left_x
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pIntrinsic[LEFT], 0, 2, atof(szBuffer));

			// cc_left_y
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pIntrinsic[LEFT], 1, 2, atof(szBuffer));
			
			continue;
		} else if(!strcmp("kc_left", szBuffer)) {
			fscanf(fp, "%s", szBuffer);
			fscanf(fp, "%s", szBuffer);
			
			// kc_left_1
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pDistCoef[LEFT], 0, 0, atof(szBuffer));
			
			// kc_left_2
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pDistCoef[LEFT], 0, 1, atof(szBuffer));

			// kc_left_3
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pDistCoef[LEFT], 0, 2, atof(szBuffer));
			
			// kc_left_4
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pDistCoef[LEFT], 0, 3, atof(szBuffer));

			// kc_left_5
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pDistCoef[LEFT], 0, 4, atof(szBuffer));

			continue;
		} else if(!strcmp("fc_right", szBuffer)) {

			fscanf(fp, "%s", szBuffer);
			fscanf(fp, "%s", szBuffer);
			
			// fc_right_x
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pIntrinsic[RIGHT], 0, 0, atof(szBuffer));

			// fc_right_y
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pIntrinsic[RIGHT], 1, 1, atof(szBuffer));
			continue;
		} else if(!strcmp("cc_right", szBuffer)) {
			fscanf(fp, "%s", szBuffer);
			fscanf(fp, "%s", szBuffer);			

			// cc_right_x
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pIntrinsic[RIGHT], 0, 2, atof(szBuffer));

			// cc_right_y
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pIntrinsic[RIGHT], 1, 2, atof(szBuffer));

			continue;
		} else if(!strcmp("kc_right", szBuffer)) {
			fscanf(fp, "%s", szBuffer);
			fscanf(fp, "%s", szBuffer);
			
			// kc_right_1
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pDistCoef[RIGHT], 0, 0, atof(szBuffer));

			// kc_right_2
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pDistCoef[RIGHT], 0, 1, atof(szBuffer));
			
			// kc_right_3
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pDistCoef[RIGHT], 0, 2, atof(szBuffer));
			
			// kc_right_4
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pDistCoef[RIGHT], 0, 3, atof(szBuffer));

			// kc_right_5
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pDistCoef[RIGHT], 0, 4, atof(szBuffer));

			continue;
		} else if(!strcmp("om", szBuffer)) {
			cv::Mat vecToR = cv::Mat(3, 1, CV_64F);

			fscanf(fp, "%s", szBuffer);
			fscanf(fp, "%s", szBuffer);

			// om_1
			fscanf(fp, "%s", szBuffer);
			vecToR.at<double>(0, 0) = atof(szBuffer);
		
			// om_2
			fscanf(fp, "%s", szBuffer);
			vecToR.at<double>(1, 0) = atof(szBuffer);
			
			// om_3
			fscanf(fp, "%s", szBuffer);
			vecToR.at<double>(2, 0) = atof(szBuffer);

			cv::Mat tmpToR;
			cv::Rodrigues(vecToR, tmpToR);

			for(int r=0; r<3; r++) {
				for(int c=0; c<3; c++) {
					cvmSet(m_pRotation, r, c, tmpToR.at<double>(r, c));
				}
			}

			continue;
		} else if(!strcmp("T", szBuffer)) {
			fscanf(fp, "%s", szBuffer);
			fscanf(fp, "%s", szBuffer);

			// T_1
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pTranslation, 0, 0, atof(szBuffer));
			
			// T_2
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pTranslation, 1, 0, atof(szBuffer));

			// T_3
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pTranslation, 2, 0, atof(szBuffer));

			continue;
		} else if(!strcmp("h_left", szBuffer)) {
			fscanf(fp, "%s", szBuffer);
			fscanf(fp, "%s", szBuffer);

			// H_1
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pH[LEFT], 0, 0, atof(szBuffer));

			// H_2
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pH[LEFT], 0, 1, atof(szBuffer));

			// H_3
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pH[LEFT], 0, 2, atof(szBuffer));

			// H_1
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pH[LEFT], 1, 0, atof(szBuffer));

			// H_2
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pH[LEFT], 1, 1, atof(szBuffer));

			// H_3
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pH[LEFT], 1, 2, atof(szBuffer));

			// H_1
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pH[LEFT], 2, 0, atof(szBuffer));

			// H_2
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pH[LEFT], 2, 1, atof(szBuffer));

			// H_3
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pH[LEFT], 2, 2, atof(szBuffer));

			continue;
		} else if(!strcmp("h_right", szBuffer)) {
			fscanf(fp, "%s", szBuffer);
			fscanf(fp, "%s", szBuffer);

			// H_1
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pH[RIGHT], 0, 0, atof(szBuffer));

			// H_2
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pH[RIGHT], 0, 1, atof(szBuffer));

			// H_3
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pH[RIGHT], 0, 2, atof(szBuffer));

			// H_1
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pH[RIGHT], 1, 0, atof(szBuffer));

			// H_2
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pH[RIGHT], 1, 1, atof(szBuffer));

			// H_3
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pH[RIGHT], 1, 2, atof(szBuffer));

			// H_1
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pH[RIGHT], 2, 0, atof(szBuffer));

			// H_2
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pH[RIGHT], 2, 1, atof(szBuffer));

			// H_3
			fscanf(fp, "%s", szBuffer);
			cvmSet(m_pH[RIGHT], 2, 2, atof(szBuffer));

			continue;
		}
	}

	// 결과 출력해보기
	printf("\n\n");
	printf("_______________________________________________________________________________\n");
	printf("# Stereo Calibration Result :\n");
	_PrintMatrix("Left Intrinsic", m_pIntrinsic[LEFT]);
	_PrintMatrix("Right Intrinsic", m_pIntrinsic[RIGHT]);
	_PrintMatrix("Left Dist-Coef.", m_pDistCoef[LEFT]);
	_PrintMatrix("Right Dist-Coef.", m_pDistCoef[RIGHT]);
	_PrintMatrix("Rotation", m_pRotation);
	_PrintMatrix("Translation", m_pTranslation);
	_PrintMatrix("Left Homography", m_pH[0]);
	_PrintMatrix("Right Homography", m_pH[1]);

	return true;
}