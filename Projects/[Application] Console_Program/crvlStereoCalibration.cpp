#include "crvlStereoCalibration.h"


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
#define LEFT		(0)
#define RIGHT		(1)


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
crvlStereoCalibration::crvlStereoCalibration(void)
{
	m_pCornerImage[0] = m_pCornerImage[1] = NULL;
	m_pDisplay = NULL;
	m_pThumbnailImageTemplate = m_pThumbnailImages = NULL;
	m_dTime = 0.;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
crvlStereoCalibration::~crvlStereoCalibration(void)
{
	_Release();
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
void
	crvlStereoCalibration::_Release(void)
{
	for(int i=0; i<2; i++) {
		if(m_pCornerImage[i]) {
			cvReleaseImage(&m_pCornerImage[i]);
			m_pCornerImage[i] = NULL;
		}
	}

	if(m_pDisplay) {
		cvReleaseImage(&m_pDisplay);
		m_pDisplay = NULL;
	}

	if(m_pThumbnailImageTemplate) {
		cvReleaseImage(&m_pThumbnailImageTemplate);
		m_pThumbnailImageTemplate = NULL;
	}

	if(m_pThumbnailImages) {
		cvReleaseImage(&m_pThumbnailImages);
		m_pThumbnailImages = NULL;
	}
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
void
crvlStereoCalibration::_ComputeObjectPoints(vector<Point3f> &inObjectPoints, Size &inBoardSize, float fDx, float fDy)
{
	// 초기화
	inObjectPoints.clear();

	for(int y=0; y<inBoardSize.height; y++) {
		for(int x=0; x<inBoardSize.width; x++) {
			inObjectPoints.push_back(Point3f(y*fDx, x*fDy, 0.f));
		}
	}
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
double
crvlStereoCalibration::_ComputeReprojectionError(vector<vector<Point2f>> &inLeftImagePoints, vector<vector<Point2f>> &inRightImagePoints, 
												 Mat &inLeftCameraMatrix, Mat &inRightCameraMatrix,
												 Mat &inLeftDistCoef, Mat &inRightDistCoef,
												 Mat &inF)
{
	// CALIBRATION QUALITY CHECK
	// because the output fundamental matrix implicitly
	// includes all the output information,
	// we can check the quality of calibration using the
	// epipolar geometry constraint: m2^t*F*m1=0
	double dError = 0.;
	int nTotalPointCount = 0;
	vector<Vec3f> lines[2];
	
	int nImageCount = (int) inLeftImagePoints.size();

	for(int i=0; i<nImageCount; i++) {
		int nPointCount = (int) inLeftImagePoints[i].size();

		Mat leftImagePts = Mat(inLeftImagePoints[i]);
		undistortPoints(leftImagePts, leftImagePts, inLeftCameraMatrix, inLeftDistCoef, Mat(), inLeftCameraMatrix);
		computeCorrespondEpilines(leftImagePts, 1, inF, lines[0]);

		Mat rightImagePts = Mat(inRightImagePoints[i]);
		undistortPoints(rightImagePts, rightImagePts, inRightCameraMatrix, inRightDistCoef, Mat(), inRightCameraMatrix);
		computeCorrespondEpilines(rightImagePts, 2, inF, lines[1]);

		for(int j=0; j<nPointCount; j++) {
			double errij = fabs(inLeftImagePoints[i][j].x*lines[1][j][0] + inLeftImagePoints[i][j].y*lines[1][j][1] + lines[1][j][2]) +
						   fabs(inRightImagePoints[i][j].x*lines[0][j][0] + inRightImagePoints[i][j].y*lines[0][j][1] + lines[0][j][2]);
			dError += errij;
		}

		nTotalPointCount += nPointCount;
	}
	
	return dError/(double) nTotalPointCount;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
bool
crvlStereoCalibration::_FindChessboardCorners(Mat &inGreyImage, Size &inBoardSize, vector<Point2f> &outCorners, int nMaxScale)
{
	// 예외처리
	if(inGreyImage.empty())
		return false;
	
	bool bFound = false;
	
	// 영상의 스케일을 변화해 가면서 코너점을 찾는다.
	for(int nScale=1; nScale<=nMaxScale; nScale++) {
		Mat tmpImg;

		if(nScale == 1) {
			tmpImg = inGreyImage;
		} else {
			resize(inGreyImage, tmpImg, Size(), nScale, nScale);
		}

// 		imshow("Input Img", tmpImg);
// 		waitKey();

		// 채스보드에서 코너점을 찾는다.
		//bFound = findChessboardCorners(tmpImg, inBoardSize, outCorners, CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_NORMALIZE_IMAGE | CALIB_CB_FAST_CHECK);
		bFound = findChessboardCorners(tmpImg, inBoardSize, outCorners, CALIB_CB_ADAPTIVE_THRESH+CALIB_CB_NORMALIZE_IMAGE);

		if(bFound) {
			if(nScale > 1) {
				Mat cornersMat(outCorners);
				cornersMat *= 1./nScale;
			}

			// 코너점을 서브픽셀 단위로 계산한다.
			cornerSubPix(inGreyImage, outCorners, Size(5,5), Size(-1,-1), TermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS, 30, 0.01));

			// 코너점을 찾은 즉시 메소드 종료!
			return true;
		}
	}

	// 여기까지 오면 코너점을 못 찾은것으로 간주!
	return false;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
void
crvlStereoCalibration::_DrawChessboardCorners(Mat &inGreyImage, Mat &outColorImage, Size &inBoardSize, vector<Point2f> &inCorners, bool bPatternWasFound)
{
	cvtColor(inGreyImage, outColorImage, CV_GRAY2BGR);

	/*{
		// 좌표축을 그린다.
		Scalar lineColor(0, 255, 0);
		Scalar fontColor(0, 255, 0);
		const int nLineSize = 5;
		const double dFontScale = 1.;
		line(outColorImage, Point(inCorners[0]), Point(inCorners[inBoardSize.width-1]), lineColor, nLineSize);
		line(outColorImage, Point(inCorners[0]), Point(inCorners[inBoardSize.width*(inBoardSize.height-1)]), lineColor, nLineSize);
		putText(outColorImage, "O", Point(inCorners[0]), FONT_HERSHEY_SIMPLEX, dFontScale, fontColor, 2, 8);
	}*/

	drawChessboardCorners(outColorImage, inBoardSize, inCorners, bPatternWasFound);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
void
crvlStereoCalibration::_PrintMatrix(char *pMessage, Mat &mat)
{
	printf("%s\n", pMessage);

	for(int r=0; r<mat.rows; r++) {
		for(int c=0; c<mat.cols; c++) {
			printf("%.12lf ", mat.at<double>(r, c));
		} printf("\n");
	} printf("\n");
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
void
crvlStereoCalibration::Init(int nImageWidth, int nImageHeight, int nChessBoardRows, int nChessBoardCols, float fDx, float fDy, char *pWorkDir)
{
	// 영상 버퍼생성
	if(m_pDisplay) {
		cvReleaseImage(&m_pDisplay);
		m_pDisplay = NULL;
	}

	m_pDisplay = cvCreateImage(cvSize(nImageWidth*2, nImageHeight), 8, 3);

	for(int i=0; i<2; i++) {
		if(m_pCornerImage[i]) {
			cvReleaseImage(&m_pCornerImage[i]);
			m_pCornerImage[i] = NULL;
		}

		m_pCornerImage[i] = cvCreateImage(cvSize(nImageWidth, nImageHeight), 8, 3);
	}

	if(m_pThumbnailImageTemplate) {
		cvReleaseImage(&m_pThumbnailImageTemplate);
		m_pThumbnailImageTemplate = NULL;
	}

	m_nThumbnailElementRows = 2;
	m_nThumbnailElementCols = 8;
	m_pThumbnailImageTemplate = cvCreateImage(cvSize(m_pDisplay->width/m_nThumbnailElementCols, m_pDisplay->height/m_nThumbnailElementCols), 8, 3);

	if(m_pThumbnailImages) {
		cvReleaseImage(&m_pThumbnailImages);
		m_pThumbnailImages = NULL;
	}

	m_pThumbnailImages = cvCreateImage(cvSize(m_pDisplay->width, m_pThumbnailImageTemplate->height*m_nThumbnailElementRows), 8, 3);

	// 내부 변수 초기화
	m_imageSize = Size(nImageWidth, nImageHeight);
	m_boardSize = Size(nChessBoardCols+1, nChessBoardRows+1);
	m_nImageCount = 0;
	sprintf(m_szWorkDir, "%s", pWorkDir);

	// 폰트객체 초기화
	cvInitFont(&m_Font, CV_FONT_VECTOR0, 0.6, 0.6, 0, 2);

	// chessboard의 3차원 점을 계산
	_ComputeObjectPoints(m_objectPts, m_boardSize, fDx, fDy);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
void
crvlStereoCalibration::Reset(void)
{
	m_imagePts[0].clear();
	m_imagePts[1].clear();
	m_objectPts.clear();
	m_nImageCount = 0;
	
	cvSetZero(m_pThumbnailImages);

	char szCommand[256];
	sprintf(szCommand, "del %s/*.bmp");
	system(szCommand);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
bool
crvlStereoCalibration::DetectCorners(IplImage *pLeftImage8bpp, IplImage *pRightImage8bpp, vector<Point2f> &leftCorners, vector<Point2f> &rightCorners, int nMaxScale)
{
	// 변수 초기화
	leftCorners.clear();
	rightCorners.clear();

	Mat leftGreyImage = Mat(pLeftImage8bpp);
	Mat rightGreyImage = Mat(pRightImage8bpp);

	// 코너점 추출
	bool bLeftSucceed = _FindChessboardCorners(leftGreyImage, m_boardSize, leftCorners, nMaxScale);
	bool bRightSucceed = _FindChessboardCorners(rightGreyImage, m_boardSize, rightCorners, nMaxScale);

	// 만약 코너점을 찾는데 모두 성공하였다면...
	if(bLeftSucceed == true && bRightSucceed == true) {
		// 결과 영상을 출력한다.
		Mat resultImage = Mat(m_pCornerImage[LEFT]);
		_DrawChessboardCorners(leftGreyImage, resultImage, m_boardSize, leftCorners, bLeftSucceed);
		namedWindow("Left Corners", CV_WINDOW_NORMAL);
		imshow("Left Corners", resultImage);

		Mat resultImage2 = Mat(m_pCornerImage[RIGHT]);
		_DrawChessboardCorners(rightGreyImage, resultImage2, m_boardSize, rightCorners, bRightSucceed);
		namedWindow("Right Corners", CV_WINDOW_NORMAL);
		imshow("Right Corners", resultImage2);

		cvWaitKey(0);

		return true;
	} else {
		cvCvtColor(pLeftImage8bpp, m_pCornerImage[LEFT], CV_GRAY2BGR);
		cvCvtColor(pRightImage8bpp, m_pCornerImage[RIGHT], CV_GRAY2BGR);

		return false;
	}
}
	

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
void
crvlStereoCalibration::Insert(vector<Point2f> &leftCorners, vector<Point2f> &rightCorners, IplImage *pLeftResultImage, IplImage *pRightResultImage, bool bSaveImage)
{
	// 추출된 결과를 저장한다.
	int nSize = m_boardSize.width*m_boardSize.height;

	for(int i=0; i<nSize; i++) {
		m_imagePts[LEFT].push_back(leftCorners[i]);
		m_imagePts[RIGHT].push_back(rightCorners[i]);
	}

	// 보정에 사용하는 영상은 저장을 함!
	if(bSaveImage) {
		char szFilePath[256];
		sprintf(szFilePath, "%s/corners_left%02d.bmp", m_szWorkDir, m_nImageCount);
		cvSaveImage(szFilePath, pLeftResultImage);

		sprintf(szFilePath, "%s/corners_right%02d.bmp", m_szWorkDir, m_nImageCount);
		cvSaveImage(szFilePath, pRightResultImage);
	}

	// 썸네일 영상 저장
	{
		_UpdateThumbnailImage(pLeftResultImage, pRightResultImage, m_nImageCount);
	}

	// 영상의 수를 한개 더 카운팅함
	m_nImageCount++;

	// 메세지 출력
	printf("Total %d image is loaded!\n", m_nImageCount);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
int
crvlStereoCalibration::Run(double &dOutRMSError, double &dOutReprojectionError, bool bShowResultMessage)
{
	// 예외처리
	if(m_nImageCount < 6) {
		dOutRMSError = -1.;
		dOutReprojectionError = -1.;
		printf("crvlStereoCalibration::Run() >> Failed! We need %d image(s) more!\n", 6-m_nImageCount);
		return -1;
	}
	
	// 컨테이너 선언 및 설정
	vector<vector<Point3f>> objectPoints;
	vector<vector<Point2f>> imagePoints[2];
	
	objectPoints.resize(m_nImageCount);
	imagePoints[LEFT].resize(m_nImageCount);
	imagePoints[RIGHT].resize(m_nImageCount);

	// 컨테이너에 데이터 삽입
	int nPointSize = m_boardSize.width*m_boardSize.height;
	int nCount = 0;

	for(int i=0; i<m_nImageCount; i++) {
		for(int j=0; j<nPointSize; j++) {
			objectPoints[i].push_back(m_objectPts[j]);
			imagePoints[LEFT][i].push_back(m_imagePts[LEFT][nCount]);
			imagePoints[RIGHT][i].push_back(m_imagePts[RIGHT][nCount]);
			nCount++;
		}
	}

	// 스테레오 보정
	m_cameraMatrix[LEFT] = Mat::eye(3, 3, CV_64F);
	m_cameraMatrix[RIGHT] = Mat::eye(3, 3, CV_64F);

	//dOutRMSError = stereoCalibrate(objectPoints, imagePoints[LEFT], imagePoints[RIGHT],
	//				 			   m_cameraMatrix[LEFT], m_distCoef[LEFT],
	//							   m_cameraMatrix[RIGHT], m_distCoef[RIGHT],
	//							   m_imageSize,
	//							   m_R, m_T, m_E, m_F,
	//							   //TermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS, 100, 1e-5),
	//							   TermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS, 100, 1e-6),
	//							   //0
	//							   CV_CALIB_FIX_ASPECT_RATIO +
	//							   CV_CALIB_ZERO_TANGENT_DIST +
	//							   CV_CALIB_SAME_FOCAL_LENGTH +
	//							   CV_CALIB_RATIONAL_MODEL +
	//							   CV_CALIB_FIX_K3 + CV_CALIB_FIX_K4 + CV_CALIB_FIX_K5);

	dOutRMSError = stereoCalibrate(objectPoints, imagePoints[LEFT], imagePoints[RIGHT],
								   m_cameraMatrix[LEFT], m_distCoef[LEFT],
								   m_cameraMatrix[RIGHT], m_distCoef[RIGHT],
								   m_imageSize,
								   m_R, m_T, m_E, m_F,
								   //TermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS, 100, 1e-5),
								   TermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS, 200, 1e-9), 0
								   //0
								   //CV_CALIB_FIX_ASPECT_RATIO +
								   //CV_CALIB_ZERO_TANGENT_DIST +
								   //CV_CALIB_SAME_FOCAL_LENGTH +
								   //CV_CALIB_RATIONAL_MODEL +
								   /*CV_CALIB_FIX_K3 + CV_CALIB_FIX_K4 + CV_CALIB_FIX_K5*/);

	// 재투영 오차 측정
	dOutReprojectionError = _ComputeReprojectionError(imagePoints[LEFT], imagePoints[RIGHT], m_cameraMatrix[LEFT], m_cameraMatrix[RIGHT], m_distCoef[LEFT], m_distCoef[RIGHT], m_F);

	// stereoRectifyUncalibrated
	{
		m_F2 = findFundamentalMat(Mat(m_imagePts[LEFT]), Mat(m_imagePts[RIGHT]), FM_RANSAC, 1.0, 0);
		stereoRectifyUncalibrated(Mat(m_imagePts[LEFT]), Mat(m_imagePts[RIGHT]), m_F2, m_imageSize, m_H[0], m_H[1], 3);
	}


	// 결과 출력
	if(bShowResultMessage) {
		printf("\n\n");
		printf("_______________________________________________________________________________\n");
		printf("# Stereo Calibration Result :\n");
		printf("Done with RMS error = %lf\n", dOutRMSError);
		printf("Average re-projection error = %lf\n", dOutReprojectionError);
		_PrintMatrix("Left Intrinsic", m_cameraMatrix[LEFT]);
		_PrintMatrix("Right Intrinsic", m_cameraMatrix[RIGHT]);
		_PrintMatrix("Left Dist-Coef.", m_distCoef[LEFT]);
		_PrintMatrix("Right Dist-Coef.", m_distCoef[RIGHT]);
		_PrintMatrix("Rotation", m_R);
		_PrintMatrix("Translation", m_T);
		_PrintMatrix("Essential Matrix", m_E);
		_PrintMatrix("Fundamental Matrix", m_F);
		_PrintMatrix("Fundamental Matrix2", m_F2);
		//_PrintMatrix("Left Homography", m_H[0]);
		//_PrintMatrix("Right Homography", m_H[1]);
	}

	return m_nImageCount;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
IplImage*
crvlStereoCalibration::GetCornerImagePtr(int nIdx)
{
	return m_pCornerImage[nIdx];
}



//IplImage*				GetThumbnailImagePtr(void);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
IplImage*
crvlStereoCalibration::GetDisplayImagePtr(void)
{
	cvSetImageROI(m_pDisplay, cvRect(0, 0, m_pCornerImage[0]->width, m_pCornerImage[0]->height));
	cvCopy(m_pCornerImage[0], m_pDisplay);
	cvResetImageROI(m_pDisplay);

	cvSetImageROI(m_pDisplay, cvRect(m_pCornerImage[0]->width, 0, m_pCornerImage[1]->width, m_pCornerImage[1]->height));
	cvCopy(m_pCornerImage[1], m_pDisplay);
	cvResetImageROI(m_pDisplay);

	// 타이머 정지
	m_dTime = (double)cvGetTickCount() - m_dTime;
	m_dTime = m_dTime/(cvGetTickFrequency()*1000);

	sprintf(m_szBuffer, "%g", m_dTime);
	m_dTime = atof(m_szBuffer);

	// 타이머 출력
	sprintf(m_szBuffer, "%.2lfms (%.1lffps)", m_dTime, 1000./m_dTime);
	cvPutText(m_pDisplay, m_szBuffer, cvPoint(12, 27), &m_Font, CV_RGB(0, 0, 0));
	cvPutText(m_pDisplay, m_szBuffer, cvPoint(10, 25), &m_Font, CV_RGB(0, 255, 0));

	// 타이머 시작
	m_dTime = (double)cvGetTickCount();

	return m_pDisplay;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
bool
crvlStereoCalibration::_UpdateThumbnailImage(IplImage *pLeftResultImage, IplImage *pRightResultImage, int nCount)
{
	// 영상 생성
	cvSetImageROI(m_pDisplay, cvRect(0, 0, pLeftResultImage->width, pLeftResultImage->height));
	cvCopy(pLeftResultImage, m_pDisplay);
	cvResetImageROI(m_pDisplay);

	cvSetImageROI(m_pDisplay, cvRect(pLeftResultImage->width, 0, pRightResultImage->width, pRightResultImage->height));
	cvCopy(pRightResultImage, m_pDisplay);
	cvResetImageROI(m_pDisplay);

	// 썸네일 템플릿에 리사이징
	cvResize(m_pDisplay, m_pThumbnailImageTemplate);

	// 리사이징 된 영상을 썸네일 리스트에 저장
	int nRows = nCount / m_nThumbnailElementCols;
	int nCols = nCount % m_nThumbnailElementCols;
	
	if(nRows < m_nThumbnailElementRows && nCols < m_nThumbnailElementCols) {
		cvSetImageROI(m_pThumbnailImages, cvRect(m_pThumbnailImageTemplate->width*nCols, m_pThumbnailImageTemplate->height*nRows, m_pThumbnailImageTemplate->width, m_pThumbnailImageTemplate->height));
		cvCopy(m_pThumbnailImageTemplate, m_pThumbnailImages);
		cvResetImageROI(m_pThumbnailImages);
		return true;
	} else {
		return false;
	}
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
bool
crvlStereoCalibration::SaveToFile(char *pFilePath, double reprojectionError)
{
	// 파일 열기
	FILE *fp = fopen(pFilePath, "w+");

	// 예외처리
	if(!fp) {
		printf("crvlStereoCalibration::SaveToFile() >> File openning failed!\n");
		return false;
	}

	// 헤더 출력
	fprintf(fp, "Stereo calibration parameters after optimization:\n\n\n");
	
	fprintf(fp, "Intrinsic parameters of left camera:\n\n");
	fprintf(fp, "Focal Length:          fc_left = [ %.16lf   %.16lf ] ? [ 0.00000   0.00000 ]\n", m_cameraMatrix[LEFT].at<double>(0, 0), m_cameraMatrix[LEFT].at<double>(1, 1));
	fprintf(fp, "Principal point:       cc_left = [ %.16lf   %.16lf ] ? [ 0.00000   0.00000 ]\n", m_cameraMatrix[LEFT].at<double>(0, 2), m_cameraMatrix[LEFT].at<double>(1, 2));
	fprintf(fp, "Skew:             alpha_c_left = [ 0.00000 ] ? [ 0.00000  ]   => angle of pixel axes = 90.00000 ? 0.00000 degrees\n");
	fprintf(fp, "Distortion:            kc_left = [ %.16lf   %.16lf   %.16lf   %.16lf   %.16lf ] ? [ 0.00000   0.00000   0.00000   0.00000  0.00000 ]\n\n\n", m_distCoef[LEFT].at<double>(0, 0), m_distCoef[LEFT].at<double>(0, 1), m_distCoef[LEFT].at<double>(0, 2), m_distCoef[LEFT].at<double>(0, 3), m_distCoef[LEFT].at<double>(0, 4));
	
	fprintf(fp, "Intrinsic parameters of right camera:\n\n");
	fprintf(fp, "Focal Length:          fc_right = [ %.16lf   %.16lf ] ? [ 0.00000   0.00000 ]\n", m_cameraMatrix[RIGHT].at<double>(0, 0), m_cameraMatrix[RIGHT].at<double>(1, 1));
	fprintf(fp, "Principal point:       cc_right = [ %.16lf   %.16lf ] ? [ 0.00000   0.00000 ]\n", m_cameraMatrix[RIGHT].at<double>(0, 2), m_cameraMatrix[RIGHT].at<double>(1, 2));
	fprintf(fp, "Skew:             alpha_c_right = [ 0.00000 ] ? [ 0.00000  ]   => angle of pixel axes = 90.00000 ? 0.00000 degrees\n");
	fprintf(fp, "Distortion:            kc_right = [ %.16lf   %.16lf   %.16lf   %.16lf   %.16lf ] ? [ 0.00000   0.00000   0.00000   0.00000  0.00000 ]\n\n\n", m_distCoef[RIGHT].at<double>(0, 0), m_distCoef[RIGHT].at<double>(0, 1), m_distCoef[RIGHT].at<double>(0, 2), m_distCoef[RIGHT].at<double>(0, 3), m_distCoef[RIGHT].at<double>(0, 4));

	fprintf(fp, "Extrinsic parameters (position of right camera wrt left camera):\n\n");
	
	Mat tmpRtoVec;
	Rodrigues(m_R, tmpRtoVec);
	_PrintMatrix("tmpRtoVec", tmpRtoVec);
	fprintf(fp, "Rotation vector:             om = [ %.16lf   %.16lf   %.16lf ] ? [ 0.00000   0.00000   0.00000 ]\n", tmpRtoVec.at<double>(0, 0), tmpRtoVec.at<double>(1, 0), tmpRtoVec.at<double>(2, 0));
	fprintf(fp, "Translation vector:           T = [ %.16lf   %.16lf   %.16lf ] ? [ 0.00000   0.00000   0.00000 ]\n", m_T.at<double>(0, 0), m_T.at<double>(1, 0), m_T.at<double>(2, 0));

	fprintf(fp, "\nRe-projection Error: %f\n", reprojectionError);
	// 파일 닫기
	fclose(fp);

	return true;
}