/*
@@ -This calibrates images loaded from a file
*/
#include "crvlStereoCalibration.h"


#define LEFT_PREFIX					"left_"
#define RIGHT_PREFIX				"right_"
#define IMAGE_FILE_PATH				"../../data/160325_ETRI"
#define LEFT_IMAGE_FILE_PATH		"../../data/160325_ETRI/"
#define RIGHT_IMAGE_FILE_PATH		"../../data/160325_ETRI/"
#define STEREO_CAL_PATH				"../../data/160325_ETRI/StereoCalibrationParams_2x (OpenCV).txt"

#define CHESSBOARD_ROWS				(4)
#define CHESSBOARD_COLS				(5)
#define CHESSBOARD_SQUARE_SIZE		(30.0)
#define IMAGE_WIDTH					(1280)
#define IMAGE_HEIGHT				(1280)
#define IMAGE_START_INDEX			(0)
#define IMAGE_END_INDEX				(19)

int main()
{
	char szFilePath[512];
	vector<Point2f> corners[2];

	const float fDx = CHESSBOARD_SQUARE_SIZE;
	const float fDy = CHESSBOARD_SQUARE_SIZE;

	crvlStereoCalibration scContext;
	scContext.Init(IMAGE_WIDTH, IMAGE_HEIGHT, CHESSBOARD_ROWS, CHESSBOARD_COLS, fDx, fDy, IMAGE_FILE_PATH);

	for(int i=IMAGE_START_INDEX; i<=IMAGE_END_INDEX; i++) {
		
		sprintf(szFilePath, "%s/%s%04d.bmp", LEFT_IMAGE_FILE_PATH, LEFT_PREFIX, i);
		IplImage *pLeftImage = cvLoadImage(szFilePath, 0);

		if (!pLeftImage)
		{
			printf("No left image\n");
			return -1;
		}

		sprintf(szFilePath, "%s/%s%04d.bmp", RIGHT_IMAGE_FILE_PATH, RIGHT_PREFIX, i);
		IplImage *pRightImage = cvLoadImage(szFilePath, 0);
		if (!pRightImage)
		{
			printf("No right image\n");
			return -1;
		}

		bool bSucceed = scContext.DetectCorners(pLeftImage, pRightImage, corners[0], corners[1]);

		if(bSucceed) {
			scContext.Insert(corners[0], corners[1], scContext.GetCornerImagePtr(0), scContext.GetCornerImagePtr(1), true);
		}

		if(pLeftImage) {
			cvReleaseImage(&pLeftImage);
		}

		if(pRightImage) {
			cvReleaseImage(&pRightImage);
		}
	}

	double dRMS, dReprojectionError;
	scContext.Run(dRMS, dReprojectionError, true);
	scContext.SaveToFile(STEREO_CAL_PATH, dReprojectionError);

	return 0;
}