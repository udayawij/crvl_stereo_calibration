#pragma once

#include <cv.h>
#include <highgui.h>

using namespace cv;


class crvlStereoCalibration
{
public:
	crvlStereoCalibration(void);
	~crvlStereoCalibration(void);

	void					Init(int nImageWidth, int nImageHeight, int nChessBoardRows, int nChessBoardCols, float fDx, float fDy, char *pWorkDir = ".");
	void					Reset(void);

	bool					DetectCorners(IplImage *pLeftImage8bpp, IplImage *pRightImage8bpp, vector<Point2f> &leftCorners, vector<Point2f> &rightCorners, int nMaxScale = 2);
	void					Insert(vector<Point2f> &leftCorners, vector<Point2f> &rightCorners, IplImage *pLeftResultImage, IplImage *pRightResultImage, bool bSaveImage = true);
	int						Run(double &dOutRMSError, double &dOutReprojectionError, bool bShowResultMessage = true);

	bool					SaveToFile(char *pFilePath, double dreprojectionError = 0.);

	IplImage*				GetCornerImagePtr(int nIdx);
	IplImage*				GetDisplayImagePtr(void);
	IplImage*				GetThumbnailImagePtr(void);


protected:
	void					_Release(void);
	void					_ComputeObjectPoints(vector<Point3f> &inObjectPoints, Size &inBoardSize, float fDx, float fDy);
	double					_ComputeReprojectionError(vector<vector<Point2f>> &inLeftImagePoints, vector<vector<Point2f>> &inRightImagePoints, 
													  Mat &inLeftCameraMatrix, Mat &inRightCameraMatrix,
													  Mat &inLeftDistCoef, Mat &inRightDistCoef,
													  Mat &inF);
	bool					_FindChessboardCorners(Mat &inGreyImage, Size &inBoardSize, vector<Point2f> &outCorners, int nMaxScale = 2);
	void					_DrawChessboardCorners(Mat &inGreyImage, Mat &outColorImage, Size &inBoardSize, vector<Point2f> &inCorners, bool bPatternWasFound);
	void					_PrintMatrix(char *pMessage, Mat &mat);
	bool					_UpdateThumbnailImage(IplImage *pLeftResultImage, IplImage *pRightResultImage, int nCount);

protected:
	char					m_szWorkDir[256], m_szBuffer[256];
	Size					m_imageSize, m_boardSize;
	Mat						m_cameraMatrix[2], m_distCoef[2], m_R, m_T, m_E, m_F, m_F2, m_H[2];
	vector<Point2f>			m_imagePts[2];
	vector<Point3f>			m_objectPts;
	int						m_nImageCount, m_nThumbnailElementRows, m_nThumbnailElementCols;
	IplImage				*m_pCornerImage[2], *m_pDisplay, *m_pThumbnailImageTemplate, *m_pThumbnailImages;

	double					m_dTime;
	CvFont					m_Font;
};
