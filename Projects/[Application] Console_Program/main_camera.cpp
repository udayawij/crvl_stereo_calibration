/*
@@ -This calibrates stereo cameras online
*/
#include "crvlFlea2.h"

#define LEFT_FLEA2_SERIAL	(6381083)
#define RIGHT_FLEA2_SERIAL	(10510978)

#define IMAGE_WIDTH			(640)
#define IMAGE_HEIGHT		(480)

#define CHESSBOARD_ROWS		(4)
#define CHESSBOARD_COLS		(5)

#define CORNER_IMAGE_FILE_PATH			"../../data/Chessboard Corners/Corner Images"
#define IMAGE_FILE_PATH_RECTIFIED		"../../data/Chessboard Corners/Stereo_Rectified"
#define STEREO_CAL_PATH					"../../data/Stereo Calibration Parameters.txt"

int main(){

	// Path initialization
	char szFilePath[512];
	vector<Point2f> corners[2];

	// Checker board pattern size
	// 	const float fDx = 24.71f;
	// 	const float fDy = 24.66f;
	const float fDx = 35.f;
	const float fDy = 35.f;

	// Creating the instance of the calibration class
	crvlStereoCalibration scContext;
	scContext.Init(IMAGE_WIDTH, IMAGE_HEIGHT, CHESSBOARD_ROWS, CHESSBOARD_COLS, fDx, fDy, CORNER_IMAGE_FILE_PATH);

	// Initializing the camera instances
	crvlFlea2 fleaContext[2];
	if(!fleaContext[0].Init(LEFT_FLEA2_SERIAL, CRVL_ENUM_FLEA2_RESOLUTION_640x480, FLYCAPTURE_FRAMERATE_30)) {
		printf("LEFT-Flea2 initialization is failed!\n");
		return -1;
	}

	if(!fleaContext[1].Init(RIGHT_FLEA2_SERIAL, CRVL_ENUM_FLEA2_RESOLUTION_640x480, FLYCAPTURE_FRAMERATE_30)) {
		printf("RIGHT-Flea2 initialization is failed!\n");
		return -1;
	}

	while(1) {
		// Acquire images
		IplImage *pLeftImagePtr = fleaContext[0].GrabGrayImage();
		IplImage *pRightImagePtr = fleaContext[1].GrabGrayImage();

		// Detecting the corners of the checker board pattern
		bool bSucceed = scContext.DetectCorners(pLeftImagePtr, pRightImagePtr, corners[0], corners[1], 1);

		// Displaying the results
		cvShowImage("Display", scContext.GetDisplayImagePtr());

		// Wait for the user's key
		int nKey = cvWaitKey(5) & 255;
		int imgCount = 0;

		// If Esc key is pressed, quit the program
		if(nKey == 27) {
			break;

		// Else if the Space bar is pressed, capture and save the corners of the pattern board image
		} else if(nKey == ' ') {
			scContext.Insert(corners[0], corners[1], scContext.GetCornerImagePtr(0), scContext.GetCornerImagePtr(1), true);
			char szFilePath[256];
			sprintf(szFilePath, "%s/2015.07.15/Img_left%02d.bmp", CORNER_IMAGE_FILE_PATH, imgCount);
			cvSaveImage(szFilePath, pLeftImagePtr);
			sprintf(szFilePath, "%s/2015.07.15/Img_right%02d.bmp", CORNER_IMAGE_FILE_PATH, imgCount);
			cvSaveImage(szFilePath, pRightImagePtr);
			imgCount++;
		
		// Else if C key is pressed, progress the calibration
		} else if(nKey == 'c' || nKey == 'C') {
			double dRMS, dReprojectionError;
			int nCount = scContext.Run(dRMS, dReprojectionError, true);

			if(nCount > 0) {
				scContext.SaveToFile(STEREO_CAL_PATH, dReprojectionError);

				// Creating the instance of the stereo rectify class
				crvlStereoRectifier srContext;
				srContext.Init(IMAGE_WIDTH, IMAGE_HEIGHT, STEREO_CAL_PATH);

				// Reading the images at the same time
				for(int i=0; i<nCount; i++) {
					// Loading the images
					sprintf(szFilePath, "%s/Corners_Left%02d.bmp", CORNER_IMAGE_FILE_PATH, i);
					IplImage *pLeftImage = cvLoadImage(szFilePath, 1);

					sprintf(szFilePath, "%s/Corners_Right%02d.bmp", CORNER_IMAGE_FILE_PATH, i);
					IplImage *pRightImage = cvLoadImage(szFilePath, 1);

					// Calibration
					if (!pLeftImage || !pRightImage)
					{
						printf("No images are loaded\n");
						return -1;
					}
					srContext.MakeRectifiedImage(pLeftImage, pRightImage);

					// Saving the rectified images
					sprintf(szFilePath, "%s/Stereo_Rectified%02d.bmp", IMAGE_FILE_PATH_RECTIFIED, i);
					cvSaveImage(szFilePath, srContext.GetRectifiedStereoImagePtr(true));

					// Releasing the ipl images from the memory
					if(pLeftImage) {
						cvReleaseImage(&pLeftImage);
					}

					if(pRightImage) {
						cvReleaseImage(&pRightImage);
					}
				}
			}
		} 
		else if(nKey == 'r' || nKey == 'R') {
			scContext.Reset();
		}
	}


	return 0;
}