#pragma once

#include <iostream>
#include <cv.h>
#include <highgui.h>
#include <vector>
#include <tchar.h>

#include "pgrflycapture.h"
#include "crvlStereoCalibration.h"
#include "crvlStereoRectifier.h"

using namespace std;
using namespace cv;

typedef enum crvlEnumFLEA2Resolution {
	CRVL_ENUM_FLEA2_RESOLUTION_320x240 = 0,
	CRVL_ENUM_FLEA2_RESOLUTION_640x480,
	CRVL_ENUM_FLEA2_RESOLUTION_800x600,
	CRVL_ENUM_FLEA2_RESOLUTION_1024x768,
	CRVL_ENUM_FLEA2_RESOLUTION_1280x960,
} crvlEnumFLEA2Resolution;


class crvlFlea2
{
public:
	crvlFlea2(void);
	~crvlFlea2(void);

	bool						Init(unsigned long nSerialNum, crvlEnumFLEA2Resolution enumResolution, FlyCaptureFrameRate enumFrameRate);

	IplImage*					GrabColorImage(void);
	IplImage*					GrabGrayImage(void);

	IplImage*					GetColorImagePtr(void);
	IplImage*					GetGrayImagePtr(void);

	void						SaveToFile(char *pFilePath);


protected:
	bool						Free(void);

	bool						m_bInit;

	int							m_nWidth, m_nHeight, m_nChannel;

	FlyCaptureContext			m_fcContext;
	FlyCaptureImage				m_fcImageOriginal, m_fcImageConverted;
	FlyCaptureError				m_fcError;

	crvlEnumFLEA2Resolution		m_Resolution;

	IplImage					*m_pColorImage, *m_pColorImage320x240, *m_pGrayImage, *m_pGrayImage320x240;
};
