#pragma once

#include <cv.h>
#include <highgui.h>


class crvlStereoRectifier
{
public:
	crvlStereoRectifier(void);
	~crvlStereoRectifier(void);

	// 초기화
	bool						Init(int nWidth, int nHeight, char *pCalibrationFilePath);

	// Rectification 영상 생성
	void						MakeRectifiedImage(IplImage *pLeftImage, IplImage *pRightImage);

	// Rectification 영상 가져오기
	IplImage*					GetRectifiedImagePtr(int nCameraIdx);

	// Rectification 된 카메라의 파라미터 리턴
	double						GetFocalLength(void)							{return m_dFocalLength; }
	double						GetImageCenterX(void)							{return m_dCenterX; }
	double						GetImageCenterY(void)							{return m_dCenterY; }
	double						GetBaseline(void)								{return m_dBaseline; }

	// Rectification 결과 확인
	IplImage*					GetRectifiedStereoImagePtr(bool bFlagDrawEpipolarLine = false, int nStartY = 3, int nInterval = 20, CvScalar color = CV_RGB(255, 0, 0));


protected:
	bool						_ParseCalibFile(char *pFilePath);
	void						_PrintMatrix(char *pMessage, CvMat *pMat);


protected:
	int							m_nWidth, m_nHeight;
	double						m_dTime;
	char						m_szBuffer[256];

	CvFont						m_Font;
	IplImage					*m_pStereoImage, *m_pRectifiedImage[2];

	// rectification을 위한 변수
	CvMat						*m_pIntrinsic[2], *m_pRectifiedIntrinsic;	// internal info.
	CvMat						*m_pDistCoef[2];
	CvMat						*m_pRotation, *m_pTranslation;				// external info.
	CvMat						*m_pRotInCameraCoord[2];
	CvMat						*m_pPPM[2];
	CvMat						*m_pH[2];
	CvMat						*m_pMapX[2], *m_pMapY[2];
	double						m_dFocalLength, m_dBaseline, m_dCenterX, m_dCenterY;
};
