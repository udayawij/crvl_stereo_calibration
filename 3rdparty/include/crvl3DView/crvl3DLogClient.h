// 프로그램 작성자 : 최성인
// 2010-12-01
// comment: MAX_SUPPORT_POINT_SIZE가 기본적으로 150000을 사용하면 스택오버플로가 발생하므로
// 프로젝트 설정에서 스택 커밋과 예약 사이즈를 늘려야 함 (10000000정도? 아님 그 이상)

#pragma once
#include <Windows.h>
//#include <vector>

//using namespace std;

typedef enum {
	CRVL_POINT_CLOUD = 0,
	CRVL_LINE_CORRESPONDENCE,
	CRVL_CLEAR_ALL
} crvlEnumDataType;


#define MAX_SUPPORT_SIZE			(640*480)


// 데이터 전송에 사용할 객체 선언
class crvl3DLoggingData
{
public:
	crvl3DLoggingData();
	~crvl3DLoggingData();
	
	void				Clear();

	void				InsertPointRef(double &dX, double &dY, double &dZ, float &fR, float &fG, float &fB);
	void				InsertPoint(double dX, double dY, double dZ, float fR, float fG, float fB);

	void				InsertLineRef(double &dSrcX, double &dSrcY, double &dSrcZ, double &dDstX, double &dDstY, double &dDstZ, float &fR, float &fG, float &fB);
	void				InsertLine(double dSrcX, double dSrcY, double dSrcZ, double dDstX, double dDstY, double dDstZ, float fR, float fG, float fB);

	void				Init(crvlEnumDataType dataType);
	
public:
	int					m_nMaxSupportSize, m_nNumOfPoint;

	double				m_arrayCoord[MAX_SUPPORT_SIZE*3];
	float				m_arrayColor[MAX_SUPPORT_SIZE*3];

	double				m_dTransformation[16];
	crvlEnumDataType	m_enumDataType;
};


// 메소드 정의
void crvlSendData(crvl3DLoggingData &data);
void crvlClearViewer(void);