% Intrinsic and Extrinsic Camera Parameters
%
% This script file can be directly excecuted under Matlab to recover the camera intrinsic and extrinsic parameters.
% IMPORTANT: This file contains neither the structure of the calibration objects nor the image coordinates of the calibration points.
%            All those complementary variables are saved in the complete matlab data file Calib_Results.mat.
% For more information regarding the calibration model visit http://www.vision.caltech.edu/bouguetj/calib_doc/


%-- Focal length:
fc = [ 1642.866269939993300 ; 1642.063480892355500 ];

%-- Principal point:
cc = [ 636.858069180206830 ; 644.826211585279000 ];

%-- Skew coefficient:
alpha_c = 0.000000000000000;

%-- Distortion coefficients:
kc = [ -0.187251495018048 ; 0.139672157565315 ; 0.000173395243553 ; 0.000398937326195 ; 0.000000000000000 ];

%-- Focal length uncertainty:
fc_error = [ 4.846364434945368 ; 4.750396424391827 ];

%-- Principal point uncertainty:
cc_error = [ 5.040679591479679 ; 4.396447245673607 ];

%-- Skew coefficient uncertainty:
alpha_c_error = 0.000000000000000;

%-- Distortion coefficients uncertainty:
kc_error = [ 0.003944234353535 ; 0.017127061111660 ; 0.000404676988448 ; 0.000457155663600 ; 0.000000000000000 ];

%-- Image size:
nx = 1280;
ny = 1280;


%-- Various other variables (may be ignored if you do not use the Matlab Calibration Toolbox):
%-- Those variables are used to control which intrinsic parameters should be optimized

n_ima = 20;						% Number of calibration images
est_fc = [ 1 ; 1 ];					% Estimation indicator of the two focal variables
est_aspect_ratio = 1;				% Estimation indicator of the aspect ratio fc(2)/fc(1)
center_optim = 1;					% Estimation indicator of the principal point
est_alpha = 0;						% Estimation indicator of the skew coefficient
est_dist = [ 1 ; 1 ; 1 ; 1 ; 0 ];	% Estimation indicator of the distortion coefficients


%-- Extrinsic parameters:
%-- The rotation (omc_kk) and the translation (Tc_kk) vectors for every calibration image and their uncertainties

%-- Image #1:
omc_1 = [ -1.957624e+00 ; -2.306654e+00 ; 1.838834e-01 ];
Tc_1  = [ 7.510924e+01 ; 6.315807e+01 ; 8.159055e+02 ];
omc_error_1 = [ 2.457512e-03 ; 3.972195e-03 ; 7.030435e-03 ];
Tc_error_1  = [ 2.511567e+00 ; 2.200448e+00 ; 2.462152e+00 ];

%-- Image #2:
omc_2 = [ 1.682553e+00 ; 2.486538e+00 ; -2.755660e-02 ];
Tc_2  = [ -4.566337e+01 ; 5.498363e+01 ; 7.965618e+02 ];
omc_error_2 = [ 2.888829e-03 ; 3.471171e-03 ; 6.590065e-03 ];
Tc_error_2  = [ 2.446492e+00 ; 2.141729e+00 ; 2.475567e+00 ];

%-- Image #3:
omc_3 = [ 1.899504e+00 ; 2.047929e+00 ; 3.487936e-01 ];
Tc_3  = [ -1.643566e+02 ; 9.424696e+01 ; 7.780823e+02 ];
omc_error_3 = [ 2.674838e-03 ; 2.021875e-03 ; 4.788057e-03 ];
Tc_error_3  = [ 2.416623e+00 ; 2.116088e+00 ; 2.748427e+00 ];

%-- Image #4:
omc_4 = [ 1.914365e+00 ; 2.152906e+00 ; 1.107020e-01 ];
Tc_4  = [ -1.500101e+02 ; -2.422165e+01 ; 8.488658e+02 ];
omc_error_4 = [ 2.565240e-03 ; 2.973050e-03 ; 5.789376e-03 ];
Tc_error_4  = [ 2.631294e+00 ; 2.292410e+00 ; 2.621298e+00 ];

%-- Image #5:
omc_5 = [ 1.231631e+00 ; 2.634500e+00 ; 2.039334e-01 ];
Tc_5  = [ -5.462365e+01 ; -2.067319e+02 ; 8.672050e+02 ];
omc_error_5 = [ 1.443269e-03 ; 3.479417e-03 ; 5.211457e-03 ];
Tc_error_5  = [ 2.706576e+00 ; 2.331632e+00 ; 2.774104e+00 ];

%-- Image #6:
omc_6 = [ -2.217989e+00 ; -2.142365e+00 ; -4.780757e-02 ];
Tc_6  = [ -1.612227e+01 ; -2.411692e+02 ; 8.778046e+02 ];
omc_error_6 = [ 3.933914e-03 ; 3.426861e-03 ; 7.601528e-03 ];
Tc_error_6  = [ 2.750581e+00 ; 2.378039e+00 ; 2.864798e+00 ];

%-- Image #7:
omc_7 = [ -5.116121e-01 ; -2.430607e+00 ; 9.613080e-02 ];
Tc_7  = [ 1.854739e+02 ; -2.522203e+02 ; 8.572475e+02 ];
omc_error_7 = [ 1.461411e-03 ; 3.167581e-03 ; 3.828494e-03 ];
Tc_error_7  = [ 2.711546e+00 ; 2.337034e+00 ; 2.942232e+00 ];

%-- Image #8:
omc_8 = [ -1.965332e+00 ; -2.000482e+00 ; 7.712517e-01 ];
Tc_8  = [ 1.047448e+02 ; -3.690422e+01 ; 8.985139e+02 ];
omc_error_8 = [ 2.802074e-03 ; 2.077596e-03 ; 4.235107e-03 ];
Tc_error_8  = [ 2.768053e+00 ; 2.410881e+00 ; 2.531389e+00 ];

%-- Image #9:
omc_9 = [ -1.795452e+00 ; -2.231341e+00 ; 8.291754e-01 ];
Tc_9  = [ 3.668968e+01 ; 1.676013e+01 ; 6.611177e+02 ];
omc_error_9 = [ 2.446581e-03 ; 2.181439e-03 ; 3.891847e-03 ];
Tc_error_9  = [ 2.025544e+00 ; 1.769658e+00 ; 1.811721e+00 ];

%-- Image #10:
omc_10 = [ 1.969599e+00 ; 1.930843e+00 ; -4.019870e-01 ];
Tc_10  = [ -5.159285e+01 ; 3.603958e+01 ; 6.202326e+02 ];
omc_error_10 = [ 2.024511e-03 ; 2.258445e-03 ; 4.019562e-03 ];
Tc_error_10  = [ 1.903256e+00 ; 1.660782e+00 ; 1.801370e+00 ];

%-- Image #11:
omc_11 = [ 1.596707e+00 ; 1.861201e+00 ; -2.001942e-01 ];
Tc_11  = [ -8.874686e+01 ; 3.660450e+01 ; 6.704613e+02 ];
omc_error_11 = [ 1.947572e-03 ; 2.360463e-03 ; 3.546068e-03 ];
Tc_error_11  = [ 2.063689e+00 ; 1.805427e+00 ; 1.993370e+00 ];

%-- Image #12:
omc_12 = [ 2.179267e+00 ; 1.765740e+00 ; 5.006121e-01 ];
Tc_12  = [ -9.179625e+01 ; -3.908770e+01 ; 6.151134e+02 ];
omc_error_12 = [ 2.583038e-03 ; 1.643817e-03 ; 4.184628e-03 ];
Tc_error_12  = [ 1.900760e+00 ; 1.654082e+00 ; 1.934432e+00 ];

%-- Image #13:
omc_13 = [ -2.334587e+00 ; -1.898158e+00 ; -6.037460e-01 ];
Tc_13  = [ -8.308302e+01 ; -1.222559e+02 ; 5.929027e+02 ];
omc_error_13 = [ 1.853560e-03 ; 2.231310e-03 ; 4.425583e-03 ];
Tc_error_13  = [ 1.855359e+00 ; 1.604785e+00 ; 1.939631e+00 ];

%-- Image #14:
omc_14 = [ -1.966206e+00 ; -1.940235e+00 ; -7.520094e-01 ];
Tc_14  = [ -9.214967e+00 ; -1.923283e+02 ; 6.589987e+02 ];
omc_error_14 = [ 1.544094e-03 ; 2.578390e-03 ; 4.033140e-03 ];
Tc_error_14  = [ 2.074178e+00 ; 1.801369e+00 ; 2.265798e+00 ];

%-- Image #15:
omc_15 = [ -1.602349e+00 ; -2.049414e+00 ; -3.155007e-02 ];
Tc_15  = [ 1.172013e+02 ; -2.294935e+02 ; 7.361377e+02 ];
omc_error_15 = [ 2.060349e-03 ; 2.549223e-03 ; 4.239406e-03 ];
Tc_error_15  = [ 2.345423e+00 ; 2.041863e+00 ; 2.394719e+00 ];

%-- Image #16:
omc_16 = [ -1.932856e+00 ; -2.186718e+00 ; 9.313749e-03 ];
Tc_16  = [ 9.471191e+01 ; -6.932168e+01 ; 7.929159e+02 ];
omc_error_16 = [ 2.456878e-03 ; 3.556454e-03 ; 5.945521e-03 ];
Tc_error_16  = [ 2.437664e+00 ; 2.138105e+00 ; 2.480190e+00 ];

%-- Image #17:
omc_17 = [ 2.141251e+00 ; 2.256398e+00 ; -9.192039e-02 ];
Tc_17  = [ -4.078247e+01 ; 5.049882e+01 ; 8.838172e+02 ];
omc_error_17 = [ 4.369348e-03 ; 3.954981e-03 ; 9.194367e-03 ];
Tc_error_17  = [ 2.716095e+00 ; 2.370983e+00 ; 2.701324e+00 ];

%-- Image #18:
omc_18 = [ -2.148289e+00 ; -1.871322e+00 ; -4.585281e-01 ];
Tc_18  = [ -5.327470e+01 ; -9.808098e+01 ; 8.403996e+02 ];
omc_error_18 = [ 2.034735e-03 ; 2.420577e-03 ; 4.626474e-03 ];
Tc_error_18  = [ 2.590952e+00 ; 2.261065e+00 ; 2.611762e+00 ];

%-- Image #19:
omc_19 = [ -1.869700e+00 ; -2.127425e+00 ; 3.155295e-01 ];
Tc_19  = [ 8.357454e+00 ; -9.441361e+00 ; 5.988806e+02 ];
omc_error_19 = [ 2.044832e-03 ; 2.500476e-03 ; 4.071846e-03 ];
Tc_error_19  = [ 1.832208e+00 ; 1.600739e+00 ; 1.722929e+00 ];

%-- Image #20:
omc_20 = [ -1.950057e+00 ; -2.344371e+00 ; -5.109591e-01 ];
Tc_20  = [ -9.783542e+01 ; -1.800687e+02 ; 6.416111e+02 ];
omc_error_20 = [ 2.046209e-03 ; 2.510427e-03 ; 4.764191e-03 ];
Tc_error_20  = [ 2.028322e+00 ; 1.764029e+00 ; 2.141374e+00 ];

