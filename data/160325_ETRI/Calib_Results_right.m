% Intrinsic and Extrinsic Camera Parameters
%
% This script file can be directly excecuted under Matlab to recover the camera intrinsic and extrinsic parameters.
% IMPORTANT: This file contains neither the structure of the calibration objects nor the image coordinates of the calibration points.
%            All those complementary variables are saved in the complete matlab data file Calib_Results.mat.
% For more information regarding the calibration model visit http://www.vision.caltech.edu/bouguetj/calib_doc/


%-- Focal length:
fc = [ 1641.961062919139700 ; 1641.040421161223100 ];

%-- Principal point:
cc = [ 632.435921970116400 ; 642.580721075569390 ];

%-- Skew coefficient:
alpha_c = 0.000000000000000;

%-- Distortion coefficients:
kc = [ -0.187838660608321 ; 0.148982618868824 ; -0.000136660562442 ; -0.000214463628552 ; 0.000000000000000 ];

%-- Focal length uncertainty:
fc_error = [ 4.731058843898413 ; 4.666495080294074 ];

%-- Principal point uncertainty:
cc_error = [ 4.918582035294535 ; 4.464312209029583 ];

%-- Skew coefficient uncertainty:
alpha_c_error = 0.000000000000000;

%-- Distortion coefficients uncertainty:
kc_error = [ 0.004528901091587 ; 0.024625510442960 ; 0.000408755690014 ; 0.000442106927824 ; 0.000000000000000 ];

%-- Image size:
nx = 1280;
ny = 1280;


%-- Various other variables (may be ignored if you do not use the Matlab Calibration Toolbox):
%-- Those variables are used to control which intrinsic parameters should be optimized

n_ima = 20;						% Number of calibration images
est_fc = [ 1 ; 1 ];					% Estimation indicator of the two focal variables
est_aspect_ratio = 1;				% Estimation indicator of the aspect ratio fc(2)/fc(1)
center_optim = 1;					% Estimation indicator of the principal point
est_alpha = 0;						% Estimation indicator of the skew coefficient
est_dist = [ 1 ; 1 ; 1 ; 1 ; 0 ];	% Estimation indicator of the distortion coefficients


%-- Extrinsic parameters:
%-- The rotation (omc_kk) and the translation (Tc_kk) vectors for every calibration image and their uncertainties

%-- Image #1:
omc_1 = [ -1.961474e+00 ; -2.313686e+00 ; 1.699770e-01 ];
Tc_1  = [ 1.349197e+01 ; 6.135105e+01 ; 8.163797e+02 ];
omc_error_1 = [ 2.447183e-03 ; 3.734240e-03 ; 6.480536e-03 ];
Tc_error_1  = [ 2.446922e+00 ; 2.228716e+00 ; 2.450038e+00 ];

%-- Image #2:
omc_2 = [ 1.676868e+00 ; 2.479446e+00 ; -1.624249e-02 ];
Tc_2  = [ -1.070044e+02 ; 5.305608e+01 ; 7.956122e+02 ];
omc_error_2 = [ 3.175487e-03 ; 4.092742e-03 ; 7.307641e-03 ];
Tc_error_2  = [ 2.386363e+00 ; 2.183626e+00 ; 2.424216e+00 ];

%-- Image #3:
omc_3 = [ 1.892680e+00 ; 2.041787e+00 ; 3.621569e-01 ];
Tc_3  = [ -2.254576e+02 ; 9.219626e+01 ; 7.755606e+02 ];
omc_error_3 = [ 2.810401e-03 ; 2.244361e-03 ; 5.234535e-03 ];
Tc_error_3  = [ 2.355091e+00 ; 2.166689e+00 ; 2.708857e+00 ];

%-- Image #4:
omc_4 = [ 1.908288e+00 ; 2.147101e+00 ; 1.255478e-01 ];
Tc_4  = [ -2.118188e+02 ; -2.643789e+01 ; 8.460684e+02 ];
omc_error_4 = [ 2.837314e-03 ; 3.571614e-03 ; 6.771303e-03 ];
Tc_error_4  = [ 2.567644e+00 ; 2.341663e+00 ; 2.626257e+00 ];

%-- Image #5:
omc_5 = [ 1.225130e+00 ; 2.624791e+00 ; 2.139257e-01 ];
Tc_5  = [ -1.164416e+02 ; -2.088372e+02 ; 8.651687e+02 ];
omc_error_5 = [ 1.454543e-03 ; 3.918935e-03 ; 5.748738e-03 ];
Tc_error_5  = [ 2.639457e+00 ; 2.372105e+00 ; 2.769069e+00 ];

%-- Image #6:
omc_6 = [ -2.219918e+00 ; -2.148829e+00 ; -6.148433e-02 ];
Tc_6  = [ -7.804591e+01 ; -2.432705e+02 ; 8.763207e+02 ];
omc_error_6 = [ 3.995628e-03 ; 3.228787e-03 ; 8.011445e-03 ];
Tc_error_6  = [ 2.680929e+00 ; 2.418326e+00 ; 2.822917e+00 ];

%-- Image #7:
omc_7 = [ -5.107671e-01 ; -2.442337e+00 ; 9.106200e-02 ];
Tc_7  = [ 1.238344e+02 ; -2.540271e+02 ; 8.583444e+02 ];
omc_error_7 = [ 1.440284e-03 ; 2.923089e-03 ; 3.846254e-03 ];
Tc_error_7  = [ 2.652442e+00 ; 2.360073e+00 ; 2.900206e+00 ];

%-- Image #8:
omc_8 = [ -1.972127e+00 ; -2.008628e+00 ; 7.597216e-01 ];
Tc_8  = [ 4.227545e+01 ; -3.887014e+01 ; 8.989803e+02 ];
omc_error_8 = [ 2.651472e-03 ; 1.881374e-03 ; 4.254341e-03 ];
Tc_error_8  = [ 2.698966e+00 ; 2.443633e+00 ; 2.488130e+00 ];

%-- Image #9:
omc_9 = [ -1.802877e+00 ; -2.239990e+00 ; 8.187827e-01 ];
Tc_9  = [ -2.297364e+01 ; 1.521143e+01 ; 6.609873e+02 ];
omc_error_9 = [ 2.392054e-03 ; 2.062798e-03 ; 4.049757e-03 ];
Tc_error_9  = [ 1.972218e+00 ; 1.798659e+00 ; 1.783433e+00 ];

%-- Image #10:
omc_10 = [ 1.967560e+00 ; 1.926068e+00 ; -3.872835e-01 ];
Tc_10  = [ -1.108024e+02 ; 3.445338e+01 ; 6.191475e+02 ];
omc_error_10 = [ 1.848019e-03 ; 2.258619e-03 ; 4.075646e-03 ];
Tc_error_10  = [ 1.856392e+00 ; 1.699432e+00 ; 1.799273e+00 ];

%-- Image #11:
omc_11 = [ 1.594791e+00 ; 1.854095e+00 ; -1.877175e-01 ];
Tc_11  = [ -1.485381e+02 ; 3.485896e+01 ; 6.688445e+02 ];
omc_error_11 = [ 1.804642e-03 ; 2.308374e-03 ; 3.658696e-03 ];
Tc_error_11  = [ 2.016051e+00 ; 1.849040e+00 ; 1.994654e+00 ];

%-- Image #12:
omc_12 = [ 2.171839e+00 ; 1.761391e+00 ; 5.155923e-01 ];
Tc_12  = [ -1.508049e+02 ; -4.072595e+01 ; 6.132313e+02 ];
omc_error_12 = [ 2.455347e-03 ; 1.685650e-03 ; 4.301998e-03 ];
Tc_error_12  = [ 1.856515e+00 ; 1.694583e+00 ; 1.946226e+00 ];

%-- Image #13:
omc_13 = [ -2.333534e+00 ; -1.903020e+00 ; -6.212839e-01 ];
Tc_13  = [ -1.417340e+02 ; -1.238210e+02 ; 5.909148e+02 ];
omc_error_13 = [ 2.019247e-03 ; 2.073524e-03 ; 4.495582e-03 ];
Tc_error_13  = [ 1.813120e+00 ; 1.650110e+00 ; 1.940871e+00 ];

%-- Image #14:
omc_14 = [ -1.963238e+00 ; -1.946414e+00 ; -7.674671e-01 ];
Tc_14  = [ -6.858700e+01 ; -1.939085e+02 ; 6.576442e+02 ];
omc_error_14 = [ 1.673309e-03 ; 2.467832e-03 ; 4.085666e-03 ];
Tc_error_14  = [ 2.025620e+00 ; 1.837322e+00 ; 2.240048e+00 ];

%-- Image #15:
omc_15 = [ -1.603128e+00 ; -2.058925e+00 ; -4.292548e-02 ];
Tc_15  = [ 5.695892e+01 ; -2.311323e+02 ; 7.364715e+02 ];
omc_error_15 = [ 2.075997e-03 ; 2.406772e-03 ; 3.954970e-03 ];
Tc_error_15  = [ 2.279168e+00 ; 2.049692e+00 ; 2.364874e+00 ];

%-- Image #16:
omc_16 = [ -1.935723e+00 ; -2.194952e+00 ; -5.637449e-03 ];
Tc_16  = [ 3.352732e+01 ; -7.107463e+01 ; 7.931887e+02 ];
omc_error_16 = [ 2.436114e-03 ; 3.315080e-03 ; 5.723697e-03 ];
Tc_error_16  = [ 2.378017e+00 ; 2.165820e+00 ; 2.444084e+00 ];

%-- Image #17:
omc_17 = [ 2.134713e+00 ; 2.251485e+00 ; -7.849107e-02 ];
Tc_17  = [ -1.031708e+02 ; 4.839231e+01 ; 8.829090e+02 ];
omc_error_17 = [ 4.093392e-03 ; 3.997960e-03 ; 8.771549e-03 ];
Tc_error_17  = [ 2.649220e+00 ; 2.413652e+00 ; 2.633976e+00 ];

%-- Image #18:
omc_18 = [ -2.148015e+00 ; -1.878056e+00 ; -4.738864e-01 ];
Tc_18  = [ -1.149518e+02 ; -1.001462e+02 ; 8.388742e+02 ];
omc_error_18 = [ 2.165506e-03 ; 2.351926e-03 ; 4.645119e-03 ];
Tc_error_18  = [ 2.525841e+00 ; 2.304099e+00 ; 2.589702e+00 ];

%-- Image #19:
omc_19 = [ -1.873548e+00 ; -2.135560e+00 ; 3.026867e-01 ];
Tc_19  = [ -5.055603e+01 ; -1.090563e+01 ; 5.983201e+02 ];
omc_error_19 = [ 2.073568e-03 ; 2.325798e-03 ; 4.109041e-03 ];
Tc_error_19  = [ 1.785155e+00 ; 1.628772e+00 ; 1.696321e+00 ];

%-- Image #20:
omc_20 = [ -1.950052e+00 ; -2.351486e+00 ; -5.242149e-01 ];
Tc_20  = [ -1.570341e+02 ; -1.818259e+02 ; 6.395367e+02 ];
omc_error_20 = [ 2.360067e-03 ; 2.510116e-03 ; 5.177209e-03 ];
Tc_error_20  = [ 1.976962e+00 ; 1.817621e+00 ; 2.155013e+00 ];

